
// List of titles
fetch('https://jsonplaceholder.typicode.com/todos')
 	.then(response => response.json())
  	.then(json => json.map(json => json.title))
	.then(json => console.log(json));


// Retrieving a specific list
fetch('https://jsonplaceholder.typicode.com/todos/1')
 	.then(response => response.json())
	.then(json => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

fetch('https://jsonplaceholder.typicode.com/todos/1')
 	.then(response => response.json())
	.then(json => console.log(json));

// Creating a list
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: 'false',
		userId: 1
	})
})
					
.then(response => response.json())
.then(json => console.log(json));


// Updating a list using PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated To Do List Item',
		description: 'To update my to do list with a different data structure',
		status: 'Pending',
		dateCompleted: 'Pending',
		userId: 1
	})
})
					
.then(response => response.json())
.then(json => console.log(json));


// Updating a list using PATCH
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		status: 'Complete',
		dateCompleted: '07/09/21',
	})
})
					
.then(response => response.json())
.then(json => console.log(json));


// Deleting a list
fetch('https://jsonplaceholder.typicode.com/todos/1', { // include id to delete
	method: 'DELETE',
})
.then(response => response.json())
.then(json => console.log(json));
